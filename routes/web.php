<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace'=>'Admin','middleware'=>'auth'],function(){

    Route::resource('departamentos','DepartamentsController');
    Route::get('departamentos/{departament_id}/productos','DepartamentsController@products');

    Route::resource('productos','ProductsController');

    Route::resource('usuarios','UsersController');

    Route::resource('entregas','EntregaController');

});


<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            'name'=>'Product faker1',
            'stock'=>10,
            'departament_id'=>4
        ]);
        Product::create([
            'name'=>'Product faker2',
            'stock'=>10,
            'departament_id'=>4
        ]);
        Product::create([
            'name'=>'Product faker3',
            'stock'=>10,
            'departament_id'=>4
        ]);
        Product::create([
            'name'=>'Product faker4',
            'stock'=>10,
            'departament_id'=>4
        ]);
    }
}

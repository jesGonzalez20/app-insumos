<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Entrega extends Model
{

    public function departamento()
    {
        return $this->belongsTo(Departament::class,'departament_id');
    }

    public function productos($idEntrega)
    {
        return ProductoEntrega::join('products','producto_entregas.product_id','products.id')
            ->groupBy('product_id')
            ->select(
                'products.id',
                'products.name',
                DB::raw(
                    'sum(producto_entregas.cantidad) as cantidad,
                    concat("http://192.168.0.9:8000/",products.image) as image'
                )
            )->where('producto_entregas.entrega_id',$idEntrega)
            ->get();
    }

    public static function  getOrCreateDelivery($entrega)
    {
        if($entrega) {
            return Entrega::findOrFail($entrega);
        }
        return Entrega::create();
    }
}

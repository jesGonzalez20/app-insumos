<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['name','stock','description','image','compatibility','departament_id','status'];

    public function departament()
    {
        return $this->belongsTo(Departament::class,'departament_id');
    }

    public function getProducts($like){
        if($like)
            return $this->search($like);
        return $this->activos();
    }

    public function search($like)
    {
        return $this->activos()->where('name','LIKE',"%{$like}%");
    }

    public function activos()
    {
        return $this->where('status',0);
    }

    public function getByDepartament($departamentId)
    {
        return $this->where('departament_id',$departamentId);
    }


}

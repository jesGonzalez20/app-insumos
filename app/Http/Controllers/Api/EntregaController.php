<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Entrega;
use App\Models\ProductoEntrega;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntregaController extends Controller
{
    public function show($idEntrega)
    {

        $entrega = Entrega::findOrFail($idEntrega);

        $productos = ProductoEntrega::join('products','producto_entregas.product_id','products.id')
            ->groupBy('product_id')
            ->select(
                'products.id',
                'products.name',
                DB::raw(
                    'sum(producto_entregas.cantidad) as cantidad,
                    concat("http://192.168.0.9:8000/",products.image) as image'
                )
            )->where('producto_entregas.entrega_id',$idEntrega)
            ->get();

        return $productos;
    }
    public function store(Request $request)
    {

        $entrega = Entrega::getOrCreateDelivery($request->entrega_id);

        $productEntrega = new ProductoEntrega();
        $productEntrega->product_id = $request->product_id;
        $productEntrega->cantidad = $request->amount;
        $productEntrega->entrega_id = $entrega->id;
        $productEntrega->save();

        return response()->json(['msg'=>'Producto agregado correctamente','entrega'=>$entrega],201);
    }

    public function update(Request $request, $entregaId)
    {
        $entrega = Entrega::findOrFail($entregaId);
        $entrega->estatus = 'terminado';
        $entrega->departament_id = $request->departament_id;
        $entrega->save();
        return response()->json(['msg'=>'ok'],200);
    }
}

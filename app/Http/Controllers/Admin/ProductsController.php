<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Departament;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    private $nameFile;
    public function index(Request $request)
    {
        $like = $request->like;
        $product = new Product();
        $products = $product->getProducts($like)->paginate(10);
        return view('admin.products.index',['like'=>$like,'products'=>$products]);
    }

    public function create()
    {
        $departaments = Departament::all();
        return view('admin.products.create',['departaments'=>$departaments,'product'=> new Product()]);
    }

    public function store(Request $request)
    {

        if($request->hasFile('fileImage')){
            $file = $request->file('fileImage');
            $name = now()->format('y-m-d').'_'.rand(1,15000).'_image_product.png';
            \Storage::disk('local')->put('products/'.$name,\File::get($file));
            $request['image'] = 'images/products/'.$name;
        }

        Product::create($request->all());

        return redirect('productos');

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        return view('admin.products.edit',['product'=>Product::findOrFail($id),'departaments'=>Departament::all()]);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->fill($request->all())->save();
        alert()->success('La información fue guardada correctamente','Registro Exitoso');
        return back();
    }

    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->fill(['status'=>1])->save();
        return back();
    }
}
